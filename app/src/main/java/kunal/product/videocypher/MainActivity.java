package kunal.product.videocypher;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.vdocipher.aegis.media.ErrorDescription;
import com.vdocipher.aegis.media.Track;
import com.vdocipher.aegis.player.VdoPlayer;
import com.vdocipher.aegis.player.VdoPlayerFragment;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class MainActivity extends AppCompatActivity implements VdoPlayer.InitializationListener {

    private final String TAG = "PlayerActivity";
    private VdoPlayerFragment playerFragment;

    private VdoPlayer player;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        playerFragment = (VdoPlayerFragment)getFragmentManager().findFragmentById(R.id.online_vdo_player_fragment);


        playerFragment.initialize(MainActivity.this);
    }

    @Override
    public void onInitializationSuccess(VdoPlayer.PlayerHost playerHost, VdoPlayer vdoPlayer, boolean b) {
        Log.i(TAG, "onInitializationSuccess");
        this.player = vdoPlayer;
        player.setPlayWhenReady(true);

        // add a listener for playback events
        player.addPlaybackEventListener(playbackListener);

        getOTP();

    }

    @Override
    public void onInitializationFailure(VdoPlayer.PlayerHost playerHost, ErrorDescription errorDescription) {

    }

    private VdoPlayer.PlaybackEventListener playbackListener = new VdoPlayer.PlaybackEventListener() {
        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
            showToast("State Changed");
        }

        @Override
        public void onSeekTo(long l) {
            showToast("Seeking");
        }

        @Override
        public void onProgress(long l) {
            showToast("OnProgress");
        }

        @Override
        public void onBufferUpdate(long l) {
            showToast("OnBufferUpdate");
        }

        @Override
        public void onPlaybackSpeedChanged(float v) {
            showToast("onPlayBackSpeedChanged");
        }

        @Override
        public void onLoading(VdoPlayer.VdoInitParams vdoInitParams) {
            showToast("onLoading...");
        }

        @Override
        public void onLoaded(VdoPlayer.VdoInitParams vdoInitParams) {
            showToast("onLoaded");
            player.setPlayWhenReady(true);
            //player.getPlayWhenReady();
        }

        @Override
        public void onLoadError(VdoPlayer.VdoInitParams vdoInitParams, ErrorDescription errorDescription) {
            showToast("onError");
        }

        @Override
        public void onMediaEnded(VdoPlayer.VdoInitParams vdoInitParams) {
            showToast("onMediaEnded");
        }

        @Override
        public void onError(VdoPlayer.VdoInitParams vdoInitParams, ErrorDescription errorDescription) {
            showToast("onError");
        }

        @Override
        public void onTracksChanged(Track[] tracks, Track[] tracks1) {
            showToast("onTracksChanged");
        }
    };



    private void showToast(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void getOTP(){
        String url = "https://dev.vdocipher.com/api/videos/f2049f9a00e8e0a4ef5a499e3b8c3be9/otp";
        Log.d(TAG,"Login url - "+url);

        OkHttpClient client = getRESTClient();
        RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"),loadJSON("test.json"));

        final okhttp3.Request request = new okhttp3.Request.Builder()
                .url(url)
                .addHeader("Authorization","Apisecret T5bOpEp71LVRbdQ3zQYAePT79NwJ5MUYGsQBH6PD9W9UFo0GS6zDLNifhty2MSiR")
                .post(body)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {

            }

            @Override
            public void onResponse(Call call, final okhttp3.Response response) throws IOException {
                final String message = response.body().string();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.d(TAG,"Response - "+message);
                        try{
                            JSONObject jsonObject = new JSONObject(message);
                            String otp = jsonObject.getString("otp");
                            String playbackInfo = jsonObject.getString("playbackInfo");
                            VdoPlayer.VdoInitParams vdoParams = VdoPlayer.VdoInitParams.createParamsWithOtp(otp,playbackInfo);
                            player.load(vdoParams);
                        }catch (Exception e){
                            showToast(e.toString());
                        }
                    }
                });
            }
        });

    }

    public OkHttpClient getRESTClient(){
        return new OkHttpClient.Builder()
                .hostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String hostname, SSLSession session) {
                        return true;
                    }
                })
                .connectTimeout(15, TimeUnit.SECONDS)
                .readTimeout(15,TimeUnit.SECONDS)
                .writeTimeout(15,TimeUnit.SECONDS)
                .retryOnConnectionFailure(false)
                .build();
    }


    public String loadJSON(String fileName){
        String json = null;
        try {
            InputStream is = getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
            return json;
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
